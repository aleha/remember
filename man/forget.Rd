% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/remember.R
\name{forget}
\alias{forget}
\title{Remove Something from Memory}
\usage{
forget(x, list_as_list = FALSE)
}
\arguments{
\item{x}{anything.  If \code{is.list(x)} and \code{list_as_list ==
FALSE} all items from the memory list identical to one of the
items in \code{x} are removed.  Otherwise all items from the
memory list idantical to \code{x} will be removed.}

\item{list_as_list}{logical.  If \code{TRUE} the items in the
memory at matched against \code{x} otherwise the items in the
memory are checkad againts the individual items in \code{x}
Defaults to \code{FALSE}}
}
\value{
logical.  \code{TRUE} if the call altered the memory,
    \code{FALSE} otherwise
}
\description{
The memory is simply a list.  This function removes the given
object(s) from the memory.
}
\examples{
remember("Important Message")
printMemory()
forget("Important Message")
printMemory()
}
\author{
Dr. Andreas Leha
}
